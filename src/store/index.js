import { createStore } from 'vuex';

const store = createStore({
  state: {
    images: [],
  },
  mutations: {
    setImages(state, images) {
      //   console.log('MUTATION_IMAGES: ', images);
      state.images = images;
    },
    addImage(state, image) {
      state.images.push(image);
    },
    removeImage(state, image) {
      // console.log('REMOVE: ', image);
      const index = state.images.indexOf(
        state.images.find(({ uniqId }) => uniqId === image),
      );
      state.images.splice(index, 1);
    },
  },
  actions: {
    setImages({ commit }, images) {
      //   console.log('ACTION_IMAGES: ', images);
      commit('setImages', images);
    },
    addImage({ commit }, image) {
      commit('addImage', image);
    },
    removeImage({ commit }, image) {
      //   console.log('REMOVE_ACTION', image);
      commit('removeImage', image);
    },
  },
  getters: {
    getImages(state) {
      //   console.log('STORE_GETTER: ', state.images);
      return state.images;
    },
  },
});
export default store;
