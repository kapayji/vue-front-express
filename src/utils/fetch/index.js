import { message } from 'ant-design-vue';

export async function postRequestToRegOrLogin(body, routPath) {
  const response = await fetch(
    `${process.env.VUE_APP_API_URL}/auth/${routPath}`,
    {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body),
    },
  );
  const data = await response.json();
  if (!response.ok) {
    if (data.errors) {
      data.errors.map(({ msg }) => message.error(msg));
      return;
    }
    return message.error(data.message);
  }
  message.success(data.message);
  return data;
}

export async function fetchingGallery(user, token) {
  const response = await fetch(
    `${process.env.VUE_APP_API_URL}/user/${user}/gallery`,
    {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    },
  );
  const data = await response.json();
  if (!response.ok) {
    if (data.errors) {
      data.errors.map(({ msg }) => message.error(msg));
      return;
    }
    return message.error(data.message);
  }
  message.success('Галерея полизователя загружена');
  return data.Owner;
}
export async function uploadImage(user, formData, token) {
  const response = await fetch(
    `${process.env.VUE_APP_API_URL}/user/${user}/upload`,
    {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${token}`,
      },
      body: formData,
    },
  );
  const data = await response.json();
  if (!response.ok) {
    if (data.errors) {
      data.errors.map(({ msg }) => message.error(msg));
      return;
    }
    return message.error(data.message);
  }
  // const repeatFetch = fetchingGallery(user, token);
  // if (!repeatFetch) {
  //   return message.error('Не удалось загрузить');
  // }
  // console.log(data.newImage);
  message.success('Галерея полизователя обновлена');
  return data.newImage;
}
export async function removeImage(user, uniqId, token) {
  const body = {
    uniqId,
  };
  const response = await fetch(
    `${process.env.VUE_APP_API_URL}/user/${user}/remove`,
    {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(body),
    },
  );
  const data = await response.json();
  if (!response.ok) {
    if (data.errors) {
      data.errors.map(({ msg }) => message.error(msg));
      return;
    }
    return message.error(data.message);
  }
  // const repeatFetch = fetchingGallery(user, token);
  // if (!repeatFetch) {
  //   return message.error('Не удалось загрузить');
  // }
  message.success('Галерея полизователя обновлена');
  // return repeatFetch;
}
