module.exports = {
  publicPath: '/',
  productionSourceMap: false,
  assetsDir: 'assets',
  devServer: {
    disableHostCheck: true,
  },
};
